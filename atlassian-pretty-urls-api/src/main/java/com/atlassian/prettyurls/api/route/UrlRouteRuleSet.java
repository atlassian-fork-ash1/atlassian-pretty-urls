package com.atlassian.prettyurls.api.route;

import com.atlassian.plugin.servlet.filter.FilterLocation;
import com.google.common.base.MoreObjects;
import com.google.common.collect.Lists;
import com.sun.jersey.api.uri.UriTemplate;

import java.util.Collections;
import java.util.List;

import static com.atlassian.prettyurls.api.route.UrlRouteRule.ParameterMode.PASS_UNMAPPED;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

/**
 * A route rule set consists of top level meta information AND a list of rules to try and match
 *
 * @since 1.11.2
 */
public class UrlRouteRuleSet {
    private final List<UrlRouteRule> urlRouteRules;
    private final List<String> topLevelPaths;
    private final UrlRouteRuleSetKey key;
    private final FilterLocation filterLocation;
    private final RoutePredicate<UrlRouteRuleSet> predicate;

    UrlRouteRuleSet(UrlRouteRuleSetKey key, List<String> topLevelPaths, List<UrlRouteRule> urlRouteRules, final FilterLocation filterLocation, final RoutePredicate<UrlRouteRuleSet> predicate) {
        this.key = checkNotNull(key);
        this.urlRouteRules = checkNotNull(urlRouteRules);
        this.topLevelPaths = checkNotNull(topLevelPaths);
        this.filterLocation = checkNotNull(filterLocation);
        this.predicate = checkNotNull(predicate);
    }

    public List<String> getTopLevelPaths() {
        return Lists.newArrayList(topLevelPaths);
    }

    public List<UrlRouteRule> getUrlRouteRules() {
        return Lists.newArrayList(urlRouteRules);
    }

    public UrlRouteRuleSetKey getKey() {
        return key;
    }

    public FilterLocation getFilterLocation() {
        return filterLocation;
    }

    public RoutePredicate<UrlRouteRuleSet> getPredicate() {
        return predicate;
    }


    public static class Builder {
        private List<UrlRouteRule> urlRouteRules;
        private List<String> topLevelPaths;
        private UrlRouteRuleSetKey key;
        private FilterLocation filterLocation;
        private RoutePredicate<UrlRouteRuleSet> predicate;

        public Builder() {
            this.urlRouteRules = Lists.newArrayList();
            this.topLevelPaths = Lists.newArrayList();
            this.filterLocation = FilterLocation.BEFORE_DISPATCH;
            this.predicate = RoutePredicates.alwaysTrue();
        }

        public Builder setKey(final UrlRouteRuleSetKey key) {
            this.key = checkNotNull(key);
            return this;
        }

        public Builder setLocation(FilterLocation location) {
            this.filterLocation = checkNotNull(location);
            return this;
        }

        public Builder setPredicate(RoutePredicate<UrlRouteRuleSet> predicate) {
            this.predicate = checkNotNull(predicate);
            return this;
        }

        /**
         * You need to add top level paths because this is used as the "quick non regex" match to see if ANY of the
         * rules might match.  Without this the code needs to perform regex matches and that is slow and hence slow is
         * bad.
         *
         * Your inner rules should therefore contain one or more of the top level paths.
         *
         * @param path the top level path to match for AN of the rules to match
         * @return the builder
         */
        public Builder addTopLevelPath(final String path) {
            checkNotNull(path);
            topLevelPaths.add(path.startsWith("/") ? path : "/" + path);
            return this;
        }

        /**
         * Add a rule that matches a 'from' url pattern to a destination (to) url pattern.
         * @param from The UriTemplate that the route matches on
         * @param to The UriTemplate that describes the destination url
         */
        public Builder addRule(UriTemplate from, UriTemplate to) {
            urlRouteRules.add(new UrlRouteRule(from, to, Collections.<String>emptyList(), PASS_UNMAPPED));
            return this;
        }

        /**
         * Add a rule that matches a 'from' url pattern to a destination (to) url pattern.
         * @param from The UriTemplate that the route matches on
         * @param to The UriTemplate that describes the destination url
         * @param httpVerbs The http verbs that the rule applies to
         * @param routePredicate The route predicate that determines if the route should match
         * @param parameterMode The way in which query parameters are passed to the destination (to) url
         */
        public Builder addRule(UriTemplate from, UriTemplate to, List<String> httpVerbs, RoutePredicate<UrlRouteRule> routePredicate, final UrlRouteRule.ParameterMode parameterMode) {
            urlRouteRules.add(new UrlRouteRule(from, to, httpVerbs, parameterMode, routePredicate));
            return this;
        }

        /**
         * Add a rule that matches a 'from' url pattern to a destination (to) url pattern.
         * @param from The UriTemplate that the route matches on
         * @param toUriGenerator A function that returns the 'to' url from the 'from' uri template variables
         * @param httpVerbs The http verbs that the rule applies to
         */
        public Builder addRule(UriTemplate from, DestinationUrlGenerator toUriGenerator, List<String> httpVerbs) {
            urlRouteRules.add(new UrlRouteRule(from, toUriGenerator, httpVerbs, PASS_UNMAPPED));
            return this;
        }

        /**
         * Add a rule that matches a 'from' url pattern to a destination (to) url pattern.
         * @param from The UriTemplate that the route matches on
         * @param toUriGenerator A function that returns the 'to' url from the 'from' uri template variables
         * @param httpVerbs The http verbs that the rule applies to
         * @param routePredicate The route predicate that determines if the route should match
         * @param parameterMode The way in which query parameters are passed to the destination (to) url
         */
        public Builder addRule(UriTemplate from, DestinationUrlGenerator toUriGenerator, List<String> httpVerbs, RoutePredicate<UrlRouteRule> routePredicate, final UrlRouteRule.ParameterMode parameterMode) {
            urlRouteRules.add(new UrlRouteRule(from, toUriGenerator, httpVerbs, parameterMode, routePredicate));
            return this;
        }

        /**
         * Add a rule that matches a 'from' url pattern to a destination (to) url pattern.
         * @param from The UriTemplate that the route matches on
         * @param to The UriTemplate that describes the destination url
         */
        public Builder addRule(String from, String to) {
            urlRouteRules.add(new UrlRouteRule(new UriTemplate(from), new UriTemplate(to), Collections.<String>emptyList(), PASS_UNMAPPED));
            return this;
        }

        /**
         * Add a rule that matches a 'from' url pattern to a destination (to) url pattern.
         * @param from The UriTemplate that the route matches on
         * @param to The UriTemplate that describes the destination url
         * @param httpVerbs The http verbs that the rule applies to
         */
        public Builder addRule(String from, String to, List<String> httpVerbs) {
            urlRouteRules.add(new UrlRouteRule(new UriTemplate(from), new UriTemplate(to), httpVerbs, PASS_UNMAPPED));
            return this;
        }

        /**
         * Add a rule that matches a 'from' url pattern to a destination (to) url pattern.
         * @param from The UriTemplate that the route matches on
         * @param to The UriTemplate that describes the destination url
         * @param httpVerbs The http verbs that the rule applies to
         * @param routePredicate The route predicate that determines if the route should match
         */
        public Builder addRule(String from, String to, List<String> httpVerbs, RoutePredicate routePredicate) {
            urlRouteRules.add(new UrlRouteRule(new UriTemplate(from), new UriTemplate(to), httpVerbs, PASS_UNMAPPED, routePredicate));
            return this;
        }

        public UrlRouteRuleSet build() {
            checkNotNull(key, "You must provide a key to identify your rule set");
            checkState(!topLevelPaths.isEmpty(), "You must provide at least one top level path");
            return new UrlRouteRuleSet(key, topLevelPaths, urlRouteRules, filterLocation, predicate);
        }
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("key", key)
                .add("filterLocation", filterLocation)
                .add("topLevelPaths", topLevelPaths)
                .add("urlRouteRules", urlRouteRules)
                .add("predicate", predicate)
                .toString();
    }
}
