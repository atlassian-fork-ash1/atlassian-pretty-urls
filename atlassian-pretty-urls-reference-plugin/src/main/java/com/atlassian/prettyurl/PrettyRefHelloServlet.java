package com.atlassian.prettyurl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

/**
 */
public class PrettyRefHelloServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        int statusCode = toInt(httpServletRequest.getParameter("sendError"), -1);
        if (statusCode > 0) {
            httpServletResponse.sendError(statusCode);
            return;
        }


        httpServletResponse.setContentType("text/html");

        PrintWriter out = httpServletResponse.getWriter();

        StringBuilder sb = new StringBuilder();
        sb.append("<html>")
                .append("<head>")
                .append("   <META name='decorator' content='general' /> ")
                .append("</head>")
                .append("<body>")
                .append("<p>Hello World.</p>")
                .append("");

        out.println(sb);

        @SuppressWarnings("unchecked")
        Map<String, String[]> paramMap = httpServletRequest.getParameterMap();

        out.print("<p>My request URL is: <strong>" + httpServletRequest.getRequestURI() + "</strong></p>");

        out.print("<p>I was called with the following parameters : </p>" +
                "<p>");
        for (Map.Entry<String, String[]> e : paramMap.entrySet()) {
            for (String val : e.getValue()) {
                out.print("<br/>" + e.getKey() + "=" + val + "");
            }
        }
        out.print("</p>");

        out.println("</body></html>");
    }


    private int toInt(String sendError, int defaultInt) {
        try {
            return Integer.parseInt(sendError);
        } catch (NumberFormatException e) {
            return defaultInt;
        }
    }
}
