package com.atlassian.prettyurl;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

/**
 */
@Path("/json")
public class PrettyRefRestCode {

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getJSON(@Context HttpServletRequest request) {
        Map<String, String[]> parameters = new HashMap<>();
        Map<String, String[]> paramMap = request.getParameterMap();
        for (Map.Entry<String, String[]> e : paramMap.entrySet()) {
            for (String[] val : paramMap.values()) {
                parameters.put(e.getKey(), val);
            }
        }

        Map<String, Object> info = new HashMap<>();
        info.put("requestURI", request.getRequestURI());
        info.put("parameters", parameters);

        Map<String, Object> outer = new HashMap<>();
        outer.put("info", info);

        return Response.ok(outer).build();
    }

}
