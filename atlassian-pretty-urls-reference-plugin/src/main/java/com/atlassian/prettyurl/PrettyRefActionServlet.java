package com.atlassian.prettyurl;

import com.atlassian.templaterenderer.TemplateRenderer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class PrettyRefActionServlet extends HttpServlet {
    private final TemplateRenderer templateRenderer;

    public PrettyRefActionServlet(final TemplateRenderer templateRenderer) {
        this.templateRenderer = templateRenderer;
    }

    private void render(String template, Map<String, Object> context, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        templateRenderer.render(template, context, response.getWriter());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        render("/templates/prettyurl.vm", makeContext(req), resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    private Map<String, Object> makeContext(HttpServletRequest request) {
        Map<String, Object> context = new HashMap<>();

        context.put("request", request);

        context.put("fromURI", toStr(request.getAttribute("pretty.urls.fromURI")));
        context.put("toURI", toStr(request.getAttribute("pretty.urls.toURI")));

        context.put("actionURI", request.getRequestURI());
        context.put("actionMethod", toStr(request.getMethod()));
        context.put("actionContentType", toStr(request.getContentType()));

        Map<String, String[]> actionParameters = new HashMap<>();
        actionParameters.putAll(request.getParameterMap());
        context.put("actionParams", actionParameters);

        return context;
    }

    private String toStr(Object s) {
        return s == null ? "" : s.toString();
    }
}
