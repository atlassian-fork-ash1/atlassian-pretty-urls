package com.atlassian.prettyurls.filter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * SiteMesh has a bug in it that means that UNLESS some one sets the content type AFTER the SiteMesh filter has been
 * entered, then it never "sees" the fact that you want the content SiteMesh rendered.  This filter works around this
 * bug on systems where the bug exists and is mostly harmless on systems when the bug is fixed.
 *
 * This filter means that consumer code does not have to perform this trick themselves.
 */
public class PrettyUrlsSiteMeshFixupFilter extends PrettyUrlsCommonFilter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest httpServletRequest = preventDoubleInvocation(servletRequest, servletResponse, filterChain);
        if (httpServletRequest == null) {
            return;
        }

        // are we inside a SiteMesh filtered request??
        if (httpServletRequest.getAttribute("com.opensymphony.sitemesh.APPLIED_ONCE") != null) {
            // ok cause a simple and mostly harmless side effect that gives SiteMesh the right kick in the guts to actually work
            servletResponse.setContentType(servletResponse.getContentType());
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

}
