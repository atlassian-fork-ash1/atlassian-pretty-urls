package com.atlassian.prettyurls.internal.util;

/**
 * URL handling
 */
public class UrlUtils {
    public static String startWithSlash(String uri) {
        return uri.startsWith("/") ? uri : "/" + uri;
    }

    public static String removePrecedingSlash(String uri) {
        return uri.startsWith("/") ? uri.substring(1) : uri;
    }

    public static String removeTrailingSlash(String uri) {
        return uri.endsWith("/") ? uri.substring(0, uri.length() - 1) : uri;
    }

    public static String prependPath(String path, String uriStr) {
        path = removeTrailingSlash(startWithSlash(path));
        uriStr = removePrecedingSlash(uriStr);
        return path + "/" + uriStr;
    }
}
